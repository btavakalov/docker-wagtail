#!/bin/sh

. ./.env

LOCAL_DUMP_PATH='postgres/dump/'

if [ ! -f ${LOCAL_DUMP_PATH}$1 ] || [ ! ${LOCAL_DUMP_PATH}$1 ]; then
    DUMP=`date +%d-%m-%Y"_"%H_%M_%S`.sql

    echo '*****************************************************************************************************************'
    echo 'pg_dump'
    echo '*****************************************************************************************************************'
    pg_dump \
            --host=${POSTGRES_REMOTE_HOST} \
            --username=${POSTGRES_USER} \
            --verbose \
            --format=c \
            craftdepot > ${LOCAL_DUMP_PATH}${DUMP}

else
    DUMP=$1
fi

echo ${DUMP}

echo '*****************************************************************************************************************'
echo 'docker-compose start postgres'
echo '*****************************************************************************************************************'
docker-compose start postgres


echo '*****************************************************************************************************************'
echo "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '${POSTGRES_DB}' AND pid <> pg_backend_pid();"
echo '*****************************************************************************************************************'
docker-compose exec postgres psql \
    --username=${POSTGRES_USER} \
    -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '${POSTGRES_DB}' AND pid <> pg_backend_pid();"


echo '*****************************************************************************************************************'
echo 'dropdb'
echo '*****************************************************************************************************************'
docker-compose exec postgres dropdb \
    --username=${POSTGRES_USER} \
    ${POSTGRES_DB}


echo '*****************************************************************************************************************'
echo 'createdb'
echo '*****************************************************************************************************************'
docker-compose exec postgres createdb \
    --username=${POSTGRES_USER} \
    ${POSTGRES_DB}


echo '*****************************************************************************************************************'
echo 'pg_restore'
echo '*****************************************************************************************************************'
docker-compose exec postgres pg_restore \
            --verbose \
            --clean \
            --username=${POSTGRES_USER} \
            --dbname=${POSTGRES_DB} /dump/${DUMP}
