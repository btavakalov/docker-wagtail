#!/bin/sh

pip install -Ur requirements.txt

/tmp/wait-for postgres:5432 -- echo "Postgres is up"

python manage.py migrate
python manage.py collectstatic --noinput --clear -v1

/usr/bin/supervisord

#uwsgi --chdir=/app \
#    --module=${PRODUCT_NAME}.wsgi:application \
#    --master --pidfile=/tmp/project-master.pid \
#    --socket=0.0.0.0:9090 \
#    --processes=5 \
#    --uid=1000 --gid=2000 \
#    --harakiri=20 \
#    --max-requests=5000 \
#    --vacuum

#    -p 16 \
#    --disable-logging \

uwsgi --master \
    --max-requests 100 \
    --socket 0.0.0.0:9090 \
    -w ${PRODUCT_NAME}.wsgi \
    --touch-reload=/app/${PRODUCT_NAME}/wsgi.py
