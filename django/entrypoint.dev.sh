#!/bin/sh

if [ ! -f requirements.txt ]
then
    wagtail start ${PRODUCT_NAME} ./
    pip freeze > requirements.txt
fi

pip install -Ur requirements.txt

/tmp/wait-for postgres:5432 -- echo "Postgres is up"

#python manage.py makemigrations
python manage.py migrate
python manage.py loaddata app

/usr/bin/supervisord
#python manage.py shell_plus --notebook &
#python manage.py runserver 0.0.0.0:8000
