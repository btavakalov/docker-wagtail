#!/bin/sh

. ./.env

#macos
#sudo ifconfig lo0 alias ${LOCAL_HOST}

#linux
#sudo ifconfig lo ${LOCAL_HOST} up

./down.sh

#docker-compose -f docker-compose.yml -f docker-compose.${ENV_NAME}.yml pull
#docker-compose -f docker-compose.yml -f docker-compose.${ENV_NAME}.yml build --no-cache
docker-compose -f docker-compose.yml -f docker-compose.${ENV_NAME}.yml up --build
