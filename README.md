# Docker Django (Django (Wagtail) - NGINX - Postges)

## Installation

0. Clone this repo

```
git clone git@bitbucket.org:btavakalov/docker-wagtail.git my_wagtail_project
    
cd my_wagtail_project
```

1. Copy and edit `.env` 

```
cp .env.dist .env && nano .env
```

2. Clone existed project (optional, otherwise will be started new Wagtail app)
    
```
git clone git://github.com/your/wagtail_app.git app
```

3. Up containers

```
./up.sh
```

## Useful commands

### django container bash

```
docker-compose exec django bash
docker-compose start
docker-compose stop
docker-compose restart
```